from click import echo
from MenuBuilder import Command, Console
from defaultMenues import *


def echo(*args: str) -> str:
    return " ".join(args)


def main():
    menu1 = NumericMenu("men1")
    menu2 = WordMenu("wordeludidoo")
    menu1.add_entity(menu2)
    menu3 = NumericMenu("num2")
    menu2.add_entity(menu3)

    main_menu = NumericMenu("Main")
    main_menu.parent = main_menu
    main_menu.add_entity(menu1)
    main_menu.add_entity(menu2)
    main_menu.add_entity(menu3)

    echo_command = Command("echo", -1, echo)

    console = Console(main_menu)
    console.add_command(echo_command)
    console.main()


if __name__ == '__main__':
    main()
